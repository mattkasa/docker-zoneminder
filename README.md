# Docker ZoneMinder

A multi-arch Docker image for ZoneMinder containing the [Event Notification Server](https://github.com/pliablepixels/zmeventnotification) and machine learning models:

- coral_edgetpu
- tinyyolov3
- tinyyolov4
- yolov3
- yolov4

## Notes

### Database
This image assumes that you running MySQL or MariaDB separately, and that you have already initialized it using something like:

```
mysql -uroot < /usr/share/zoneminder/db/zm_create.sql
mysql -uroot -e "grant all on zm.* to 'zmuser'@localhost identified by 'zmpass';"
```

This image will not be responsible for managing your database.

### SSL

This image does not use SSL for apache2, it expects you to use an ingress like nginx to terminate SSL.
